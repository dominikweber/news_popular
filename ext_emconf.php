<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Most popular news',
	'description' => 'Tracks the most viewed articles for tx_news. Allows you to filter the news items based on time and categories.',
	'category' => 'plugin',
	'author' => 'www.dominikweber.de',
	'author_email' => 'post@dominikweber.de',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 1,
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.99.99',
			'news' => '4.0-4.99.99'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);