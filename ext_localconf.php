<?php
	
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'DominikWeber.' . $_EXTKEY,
	'News',
	array(
		'News' => 'widget',
	),
	array(
		'News' => 'widget',
	)
);

$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/News'][] = 'news_popular';

$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance( 'TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher' );
$signalSlotDispatcher->connect(
	'GeorgRinger\\News\\Controller\\NewsController',
	'detailAction',
	'DominikWeber\\NewsPopular\\Slots\\NewsControllerSlot',
	'detailActionSlot',
	true
);