<?php

namespace DominikWeber\NewsPopular\Slots;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dominik Weber <post@dominikweber.de>, www.dominikweber.de
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * News controller slot
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class NewsControllerSlot{

	/**
	 * persistenceManager
	 *
	 * @var  \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;

	/**
	 * newsRepository
	 *
	 * @var  \GeorgRinger\News\Domain\Repository\NewsRepository
	 * @inject
	 */
	protected $newsRepository;

	/**
	 * detailActionSlot
	 *
	 * @param \GeorgRinger\News\Domain\Model\News $news
	 * @param \GeorgRinger\News\Classes\Controller\NewController $parentObject
	 * 
	 * @return void
	 */
	public function detailActionSlot( $news , $parentObject ){  
		$news->setViews( intval( $news->getViews() ) + 1 );
		$this->newsRepository->update( $news );
		$this->persistenceManager->persistAll();
	} 

}