<?php

if( ! defined( 'TYPO3_MODE' ) ){
	die( 'Access denied.' );
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	$_EXTKEY,
	'Configuration/TypoScript',
	'Most popular news'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'News',
	'Most popular news'
);

\TYPO3\CMS\Core\Utility\GeneralUtility::requireOnce( \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath( $_EXTKEY ) . 'Configuration/TCA/News.php' );

$extensionName = strtolower( \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase( $_EXTKEY ) );
$pluginSignature = strtolower( $extensionName . '_News' );
$TCA['tt_content']['types']['list']['subtypes_excludelist'][ $pluginSignature ] = 'select_key';
$TCA['tt_content']['types']['list']['subtypes_addlist'][ $pluginSignature ] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue( $pluginSignature , 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/News.xml' );